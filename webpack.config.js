var packageJSON = require("./package.json");
var path = require("path");
var webpack = require("webpack");

module.exports = {
	entry: "./src/post-scheduler/post-scheduler.js",
	output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'index.js',
    libraryTarget: 'commonjs2'
  },
	module: {
		rules: [ {
			test: /\.js$/,
			include: [
				path.resolve(__dirname, "src")],
			exclude: /(node_modules|bower_components|build)/,
			use: {
				loader: "babel-loader",
				options: {
					presets: ["env"]
				}
			}
		}, {
			test: /\.css$/,
			use: [{
					loader: "style-loader"
				}, {
					loader: "css-loader"
					}]
		}]
	}
}
