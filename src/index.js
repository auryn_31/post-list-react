import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './style.css';
import registerServiceWorker from './registerServiceWorker';
import PostsSchedule from './post-scheduler/post-scheduler';

var initialPosts = [ { "deleteThisPostLink": { "href": "http://localhost:8080/posts/5a69ec357b6e63f90bedf3c", "rel": "delete", "templated": false, "variableNames": [], "variables": [] }, "largeImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/large", "rel": "largeImage", "templated": false, "variableNames": [], "variables": [] }, "license": null, "linkToPossibleUploaders": null, "smallImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/small", "rel": "smallImage", "templated": false, "variableNames": [], "variables": [] }, "source": null, "text": "Hier steht der Post text 3" }, { "deleteThisPostLink": { "href": "http://localhost:808/posts/5a69ec35d7b6e63f90bedf3c", "rel": "delete", "templated": false, "variableNames": [], "variables": [] }, "largeImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/large", "rel": "largeImage", "templated": false, "variableNames": [], "variables": [] }, "license": null, "linkToPossibleUploaders": null, "smallImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/small", "rel": "smallImage", "templated": false, "variableNames": [], "variables": [] }, "source": null, "text": "Hier steht der Post text 1" }, { "deleteThisPostLink": { "href": "http://localhost:8080/posts/5a6935d7b6e63f90bedf3c", "rel": "delete", "templated": false, "variableNames": [], "variables": [] }, "largeImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/large", "rel": "largeImage", "templated": false, "variableNames": [], "variables": [] }, "license": null, "linkToPossibleUploaders": null, "smallImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/small", "rel": "smallImage", "templated": false, "variableNames": [], "variables": [] }, "source": null, "text": "Hier steht der Post text 2" } ];


ReactDOM.render(<PostsSchedule posts={initialPosts}/>, document.getElementById('root'));
registerServiceWorker();
