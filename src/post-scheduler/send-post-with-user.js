import React from 'react';
import Icon from 'react-icons-kit';
import { ic_send } from 'react-icons-kit/md/ic_send';
import axios from 'axios';
import toastr from 'toastr/build/toastr.min.js';

class SendPostWithUser extends React.Component {

  performSendPost(url){
    toastr.options.closeButton = true;
    this.props.closeModal();
    axios.post(url)
      .then(response => {
        toastr.success("Senden des Posts hat geklappt.");
      })
      .catch(err => {
        toastr.error("Fehler beim Senden des Posts.");
        console.log(err);
      });
  }

	render() {
    console.log(this.props);
		return (
      <div>
          <div><b>Username:</b> {this.props.username}</div>
          <Icon onClick={() => this.performSendPost(this.props.sendPostLink)} icon={ic_send} size={16}/>
      </div>
    );
	}

}

export default SendPostWithUser;
