import React from 'react';
import Icon from 'react-icons-kit';
import { ic_send } from 'react-icons-kit/md/ic_send';
import { ic_delete_forever } from 'react-icons-kit/md/ic_delete_forever';
import axios from 'axios';
import Modal from 'react-responsive-modal';
import SendPostWithUser from './send-post-with-user';
import toastr from 'toastr/build/toastr.min.js';
import { connect } from "react-redux";
import { deletePost } from "./redux-handler/redux-actions";
import ImageZoom from 'react-medium-image-zoom'

class PostElement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      possibleUsers: null,
      openModal: false
    }
  }

  onOpenModal() {
    this.setState({ openModal: true });
  };

  onCloseModal() {
    this.setState({ openModal: false });
  };

  performDelete(url) {
    this.props.dispatch(deletePost(url));
    axios.delete(url)
        .then(response => {
          toastr.success("Löschen hat geklappt.");
        })
        .catch(err => {
          toastr.error("Fehler beim Löschen.");
          console.log(err);
        });
  }

  // componentDidMount() {
  //   this.testUsers();
  // }

  performSendPost(url){
    axios.get(url)
      .then(response => {
        var possibleUsers = response.data.map(user => <SendPostWithUser username={user.rel} sendPostLink={user.href} closeModal={this.onCloseModal.bind(this)}/>);
        this.setState({
          possibleUsers: possibleUsers,
          openModal: true
        });
      })
      .catch(err => console.log(err));
  }

  // testUsers() {
  //   var users = [{"rel":"FragCando","href":"http://localhost:8080/posts/5a69ec35d7b6e63f90bedf3c/publish?user=FragCando"},{"rel":"ProSoccerLover","href":"http://localhost:8080/posts/5a69ec35d7b6e63f90bedf3c/publish?user=ProSoccerLover"},{"rel":"LeiseDroid","href":"http://localhost:8080/posts/5a69ec35d7b6e63f90bedf3c/publish?user=LeiseDroid"},{"rel":"susanneebersbac","href":"http://localhost:8080/posts/5a69ec35d7b6e63f90bedf3c/publish?user=susanneebersbac"},{"rel":"LoudDroid","href":"http://localhost:8080/posts/5a69ec35d7b6e63f90bedf3c/publish?user=LoudDroid"},{"rel":"barbara84mayer","href":"http://localhost:8080/posts/5a69ec35d7b6e63f90bedf3c/publish?user=barbara84mayer"},{"rel":"annebaumgaertne","href":"http://localhost:8080/posts/5a69ec35d7b6e63f90bedf3c/publish?user=annebaumgaertne"}];
  //   var possibleUsers = users.map(user => <SendPostWithUser username={user.rel} sendPostLink={user.href} closeModal={this.onCloseModal.bind(this)}/>);
  //   this.setState({
  //     possibleUsers: possibleUsers,
  //     openModal: true
  //   });
  // }

	render() {
		return (
      <div className="post-item">
        <div className="post-image"><ImageZoom
            image={{src: this.props.post.smallImageLink.href, alt: "post"}}
            zoomImage={{src: this.props.post.largeImageLink.href, alt: "post"}}
            defaultStyles={{overlay:{backgroundColor: '#3a3a3a'}}}
           /></div>
        <p className="post-text">{this.props.post.text}</p>
        <p className="post-copyright">{this.props.post.license}</p>
        <Icon onClick={() => this.performSendPost(this.props.post.linkToPossibleUploaders.href)} className="send-button" icon={ic_send} size={32}/>
        <Icon className="delete-button" onClick={() => this.performDelete(this.props.post.deleteThisPostLink.href)} icon={ic_delete_forever} size={32}/>
        <Modal open={this.state.openModal} onClose={this.onCloseModal.bind(this)} little>
          <div className="post-send-modal">
            <h2>Send with User</h2>
            <div>{this.state.possibleUsers}</div>
          </div>
        </Modal>
      </div>
    );
	}
}

export default connect()(PostElement);
