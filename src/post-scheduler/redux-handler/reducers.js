const initialState = {
  fetching: false,
  fetched: false,
  posts: [],
  error: null
}

export const postReducer = (state = initialState, action) => {
  switch(action.type){
    case "INITIAL_POSTS":
      state = {...state, posts: action.payload};
      break;
    case "FETCH_POSTS_PENDING":
      state = {...state, fetching: true};
      break;
    case "FETCH_POSTS_FULFILLED":
      state = {...state, posts: [...state.posts].concat(action.payload), fetching: false, fetched: true};
      // state = {...state, posts: [ { "deleteThisPostLink": { "href": "http://localhost:8080/posts/5a69ec357b6e63f90bedf3c", "rel": "delete", "templated": false, "variableNames": [], "variables": [] }, "largeImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/large", "rel": "largeImage", "templated": false, "variableNames": [], "variables": [] }, "license": null, "linkToPossibleUploaders": null, "smallImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/small", "rel": "smallImage", "templated": false, "variableNames": [], "variables": [] }, "source": null, "text": "Hier steht der Post text 3" }, { "deleteThisPostLink": { "href": "http://localhost:808/posts/5a69ec35d7b6e63f90bedf3c", "rel": "delete", "templated": false, "variableNames": [], "variables": [] }, "largeImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/large", "rel": "largeImage", "templated": false, "variableNames": [], "variables": [] }, "license": null, "linkToPossibleUploaders": null, "smallImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/small", "rel": "smallImage", "templated": false, "variableNames": [], "variables": [] }, "source": null, "text": "Hier steht der Post text 1" }, { "deleteThisPostLink": { "href": "http://localhost:8080/posts/5a6935d7b6e63f90bedf3c", "rel": "delete", "templated": false, "variableNames": [], "variables": [] }, "largeImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/large", "rel": "largeImage", "templated": false, "variableNames": [], "variables": [] }, "license": null, "linkToPossibleUploaders": null, "smallImageLink": { "href": "http://localhost:8080/posts/NO_IMAGE/image/small", "rel": "smallImage", "templated": false, "variableNames": [], "variables": [] }, "source": null, "text": "Hier steht der Post text 2" } ], fetching: false, fetched: true};
      break;
    case "FETCH_POSTS_REJECTED":
      state = {...state, error: action.payload, fetching: false, fetched: false};
      break;
    case "DELETE_POST":
      let posts = [...state.posts].filter(post=> post.deleteThisPostLink.href !== action.payload);
      state = {...state, posts: posts};
      break;
    case "ADD_POST":
      state = {...state, posts: [...state.posts, action.payload]};
      break;
    default:
      break;
  }
  return state;
}
