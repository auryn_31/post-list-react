// store.subscribe(() => {
//   console.log("store changed", store.getState());
// })
//
// store.dispatch({
//   type: "FETCH_POSTS",
//   payload: axios.get("http://rest.learncode.academy/api/wsern/users")
// });

import axios from "axios";


export function loadPosts(){
  return {
    type: "FETCH_POSTS",
    payload: axios.get("http://rest.learncode.academy/api/wsern/users")
  }
}

export function deletePost(post){
  return {
    type: "DELETE_POST",
    payload: post
  }
}

export function setInitialPosts(posts) {
  return {
    type: "INITIAL_POSTS",
    payload: posts
  }
}

export function addPost(post) {
  return {
    type: "ADD_POST",
    payload: post
  }
}
