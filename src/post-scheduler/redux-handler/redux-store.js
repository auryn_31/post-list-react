import {createStore, combineReducers, applyMiddleware} from 'redux';
import {createLogger} from "redux-logger";
import promise from "redux-promise-middleware";
import {postReducer} from "./reducers";


// const tweetReducer = (state=[], action) => {
//   return state;
// }

const reducers = combineReducers({
    posts: postReducer,
    // tweets: tweetReducer
})

let middleware = applyMiddleware(promise(), createLogger());

export const store = createStore(reducers, middleware);
