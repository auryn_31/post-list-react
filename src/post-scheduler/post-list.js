import React from 'react';
import PostElement from './post-element';
import { connect } from "react-redux";
import { loadPosts, setInitialPosts } from "./redux-handler/redux-actions";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class PostList extends React.Component {

	componentWillMount(){
		if(this.props.initialPosts !== undefined) {
			this.props.dispatch(setInitialPosts(this.props.initialPosts));
		} else {
			this.props.dispatch(loadPosts());
		}
	}

	render() {
		var posts = []
		for(var post in this.props.posts){
			posts.push(<PostElement key={post} post={this.props.posts[post]} />);
		}
		return (<ReactCSSTransitionGroup transitionName="post-list" className="grid-item"  id="postList">
							{posts}
            </ReactCSSTransitionGroup>);
	}

}

function mapStateToProps(store){
	return {
		posts: store.posts.posts
	}
}

export default connect(mapStateToProps)(PostList);
