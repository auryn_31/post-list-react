import React from 'react';
import Icon from 'react-icons-kit';
import { ic_playlist_add } from 'react-icons-kit/md/ic_playlist_add';
import { ic_send } from 'react-icons-kit/md/ic_send';
import Modal from 'react-responsive-modal';
import axios from 'axios';
import toastr from 'toastr/build/toastr.min.js';
import { addPost } from './redux-handler/redux-actions';
import { connect } from "react-redux";

class CreatePost extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      post: "",
      modalOpen: false
    };
  }

  onOpenModal() {
    this.setState({ modalOpen: true });
  };

  onCloseModal() {
    this.setState({ modalOpen: false });
  };

  handleSubmit(event){
    event.preventDefault();
    const data = new FormData(event.target);
    axios.post(this.props.linkToCreate, data)
      .then(post =>{
        toastr.success("jo, gesendet wa");
        this.props.dispatch(addPost(post.data));
      })
      .catch(err => {
        console.log(err);
        toastr.error("hm, fehler");
      })
    this.onCloseModal();
  }

  render() {
    return (
      <div id="createButton">
        <Icon icon={ic_playlist_add} onClick={this.onOpenModal.bind(this)} className="grid-item" id="add-button"  size={32}/>
        <Modal open={this.state.modalOpen} onClose={this.onCloseModal.bind(this)} little>
        <form className="post-create-modal" onSubmit={this.handleSubmit.bind(this)}>
          <label htmlFor="text">Text</label>
          <input type="text" name="text" id="text"/>
          <label htmlFor="license">License: </label>
          <input type="text" name="license" id="license"/>
          <label htmlFor="source">Source: </label>
          <input type="text" name="source" id="source"/>
          <label htmlFor="binaryImage">Bild:</label>
          <input type="file" name="binaryImage" id="binaryImage"/>
          <div id="send-new-post-button"><button><Icon icon={ic_send} size={32}/></button></div>
        </form>
        </Modal>
      </div>
      );
  }
}

export default connect()(CreatePost);
