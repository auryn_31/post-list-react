import React from 'react';
import PostList from './post-list';
import CreatePost from './create-post';
import '../index.css';
import '../style.css';
import 'toastr/build/toastr.css';
import { Provider } from "react-redux";
import { store } from "./redux-handler/redux-store";

class PostsSchedule extends React.Component {

	render() {
		return (<Provider store={store}>
							<div id="main-container">
									<PostList initialPosts={this.props.posts}/>
									<CreatePost linkToCreate={this.props.linkToCreate}/>
								</div>
						</Provider>);
	}

}

export default PostsSchedule;
